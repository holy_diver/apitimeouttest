﻿namespace DDoS
{
    public abstract class Attack
    {
        public string Endpoint { get; }
        public Attack(string endpoint)
        {
            Endpoint = endpoint;
        }
        public abstract void StartAttack();
        public abstract void StopAttack();
    }
}